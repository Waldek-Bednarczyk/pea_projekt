﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEA_projekt
{
    class BranchAndBound
    {
        //sumaryczny koszt odwiedzin wszystkich miast
        int _value;
        //ilosc wierzcholkow
        int _vertices;
        int _weight = int.MinValue;
        //macierz sasiedztwa
        int[,] _matrix;
        //droga, o najmniejszej wartosci
        int[] _road;
        //lista odwiedzonych juz wierzcholkow
        List<bool> visitedNodes = new List<bool>();

        /// <summary>
        /// konstruktor przypisujacy wartosci z WindowsForm do stworzonego obiektu
        /// </summary>
        /// <param name="matrix"></param>
        public BranchAndBound(int[,] matrix)
        {
            _matrix = matrix;
            _vertices = _matrix.GetLength(0);
            _road = new int[_vertices];
        }

        /// <summary>
        /// glowna metoda wywolujaca podzial i ograniczenia
        /// </summary>
        /// <returns></returns>
        public ResultReturn CountBandB()
        {
            //wypelnienie listy wartoscia false
            visitedNodes = Enumerable.Repeat(false, _vertices).ToList();
            //zakomentowano pomiar czasu wraz z wypisywaniem go do pliku .txt
            //Stopwatch stopWatch = new Stopwatch();
            //stopWatch.Start();
            //wywolanie funkcji podzialu
            Branch(new int[_vertices], 0, 0, 0, 0);
            //stopWatch.Stop();
            //long ts = stopWatch.ElapsedMilliseconds;
            //string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //using (StreamWriter outputFile = new StreamWriter(Path.Combine(desktop, "Result.txt")))
            //{
            //    outputFile.WriteLine(ts);
            //}
            ResultReturn result = new ResultReturn { result = _value, road = _road };
            return result;
        }

        /// <summary>
        /// metoda podzialu
        /// </summary>
        /// <param name="tmpRoad"></param>
        /// <param name="index"></param>
        /// <param name="startVertex"></param>
        /// <param name="vertex"></param>
        /// <param name="weight"></param>
        private void Branch(int[] tmpRoad, int index, int startVertex, int vertex, int weight)
        {
            //pierwszy wierzcholek odwiedzony
            visitedNodes[vertex] = true;
            //jesli jakikolwiek wierzcholek nie zostal odwiedzony
            if (visitedNodes.Any(element => element == false))
            {
                for (int i = 0; i < _vertices; i++)
                {
                    //sprawdzenie czy z danego wierzcholka mozna gdzies dotrzec
                    if (visitedNodes[i] == false && _matrix[vertex, i] > 0)
                    {
                        if((Bound(vertex) + weight < _weight || _weight == int.MinValue))
                        {
                            //jest kolejna potencjalna droga
                            tmpRoad[index] = i;
                            Branch(tmpRoad, index + 1, startVertex, i, weight + _matrix[vertex, i]);
                        }
                    }
                    if (vertex == startVertex && i == _vertices - 1)
                    {
                        //zapisanie wyniku
                        _value = _weight;
                        return;
                    }
                }
                visitedNodes[vertex] = false;
                return;
            }
            else
            {
                //sprawdzenie czy nowo obliczona waga jest mniejsza
                if (_weight > weight + _matrix[vertex, startVertex])
                {
                    _weight = weight + _matrix[vertex, startVertex];
                    //zapisanie obecnie najkrotszej drogi
                    for (int i = 0; i < _vertices - 1; i++) _road[i + 1] = tmpRoad[i];
                }
                //tylko na poczatku(1 raz)
                else if (_weight == int.MinValue)
                {
                    //nowa waga
                    _weight = weight + _matrix[vertex, startVertex];
                    //kopiowanie tablicy od 2 elementu (zalozenie ze wierzcholek 0 jest zawsze pierwszy)
                    for (int i = 0; i < _vertices - 1; i++) _road[i + 1] = tmpRoad[i];
                }
                visitedNodes[vertex] = false;
                return;  
            }
        }
        /// <summary>
        /// metoda ograniczajaca
        /// </summary>
        /// <param name="vertex"></param>
        /// <returns></returns>
        private int Bound(int vertex)
        {
            int tmpValue = 0;
            for (int i = 0; i < _vertices; i++)
            {
                int min = 0;
                if (i == vertex || visitedNodes[i] == false)
                {
                    for (int j = 0; j < _vertices; j++)
                    {
                        if (i != j && visitedNodes[j] == false)
                        {
                            if(_matrix[i, j] < min) min = _matrix[i, j];
                            else if (min == 0) min = _matrix[i, j];
                        }
                    }
                }
                tmpValue += min;
                min = 0;
            }
            return tmpValue;
        }
    }
}