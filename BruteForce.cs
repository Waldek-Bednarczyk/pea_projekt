﻿using System;
using System.Diagnostics;
using System.IO;

namespace PEA_projekt
{
    class BruteForce
    {
        int _value = int.MaxValue;
        int[] _road;
        int[,] _matrix;

        /// <summary>
        /// konstruktor przypisujący wczytane dane
        /// </summary>
        /// <param name="tmpMatrix"></param>
        public BruteForce(int[,] tmpMatrix)
        {
            _matrix = tmpMatrix;
            _road = new int[_matrix.GetLength(0)];
        }
        /// <summary>
        /// glowna metoda, wywolujaca obliczenia oraz zwracajaca wynik w postaci ResultReturn
        /// </summary>
        /// <param name="vertexTab"></param>
        /// <returns></returns>
        public ResultReturn CountBF(int[] vertexTab)
        {
            //zakomentowano pomiar czasu wraz z wypisywaniem go do pliku .txt
            //Stopwatch stopWatch = new Stopwatch();
            //stopWatch.Start();
            Permutate(vertexTab);
            //stopWatch.Stop();
            //long ts = stopWatch.ElapsedMilliseconds;
            //string desktop = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            //using (StreamWriter outputFile = new StreamWriter(Path.Combine(desktop, "Result.txt")))
            //{
            //    outputFile.WriteLine(ts);
            //}
            ResultReturn result = new ResultReturn { result = _value, road = _road };
            return result;
        }
        /// <summary>
        /// metoda wykorzystujaca mechanizm 'tuple' do zamiany dwoch wartosci
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void Swap(ref int x, ref int y)
        {
            if (x == y) return;
            //tuple
            (x, y) = (y, x);
        }
        /// <summary>
        /// metoda wywolywana tylko raz, na poczatku poszukiwania permutacji
        /// </summary>
        /// <param name="list"></param>
        public void Permutate(int[] list)
        {
            int x = list.Length - 1;
            Permutate(list, 0, x);
        }
        /// <summary>
        /// glowna metoda wyznaczajaca permutacje
        /// </summary>
        /// <param name="list"></param>
        /// <param name="current"></param>
        /// <param name="max"></param>
        private void Permutate(int[] list, int current, int max)
        {
            if (current == max)
            {
                Calculate(list);
            }
            else
            {
                for (int i = current; i <= max; i++)
                {
                    Swap(ref list[current], ref list[i]);
                    Permutate(list, current + 1, max);
                    Swap(ref list[current], ref list[i]);
                }
            }
        }
        /// <summary>
        /// metoda obliczajaca droge na podstawie wyliczonych permutacji
        /// </summary>
        /// <param name="possibleList"></param>
        private void Calculate(int[] possibleList)
        {
            int tmpRoad = 0;
            for(int i = 0; i < possibleList.Length-1; i++)
            {
                tmpRoad += _matrix[possibleList[i], possibleList[i + 1]];
            }
            tmpRoad += _matrix[possibleList[possibleList.Length-1], possibleList[0]];

            if (tmpRoad < _value)
            {
                _value = tmpRoad;
                for(int i=0; i< possibleList.Length; i++)
                {
                    _road[i] = possibleList[i];
                }
            }
        }
    }
}
