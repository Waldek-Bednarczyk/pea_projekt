﻿namespace PEA_projekt
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSource = new System.Windows.Forms.Button();
            this.buttonBruteForce = new System.Windows.Forms.Button();
            this.labelRoad = new System.Windows.Forms.Label();
            this.labelResult = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonBranchAndBound = new System.Windows.Forms.Button();
            this.buttonTabu = new System.Windows.Forms.Button();
            this.textBoxTime = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonGenetic = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonSource
            // 
            this.buttonSource.Location = new System.Drawing.Point(12, 12);
            this.buttonSource.Name = "buttonSource";
            this.buttonSource.Size = new System.Drawing.Size(99, 38);
            this.buttonSource.TabIndex = 0;
            this.buttonSource.Text = "Wczytaj plik";
            this.buttonSource.UseVisualStyleBackColor = true;
            this.buttonSource.Click += new System.EventHandler(this.buttonSource_Click);
            // 
            // buttonBruteForce
            // 
            this.buttonBruteForce.Location = new System.Drawing.Point(12, 55);
            this.buttonBruteForce.Margin = new System.Windows.Forms.Padding(2);
            this.buttonBruteForce.Name = "buttonBruteForce";
            this.buttonBruteForce.Size = new System.Drawing.Size(100, 38);
            this.buttonBruteForce.TabIndex = 1;
            this.buttonBruteForce.Text = "Brute Force";
            this.buttonBruteForce.UseVisualStyleBackColor = true;
            this.buttonBruteForce.Click += new System.EventHandler(this.buttonBruteForce_Click);
            // 
            // labelRoad
            // 
            this.labelRoad.Location = new System.Drawing.Point(133, 55);
            this.labelRoad.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelRoad.Name = "labelRoad";
            this.labelRoad.Size = new System.Drawing.Size(236, 94);
            this.labelRoad.TabIndex = 2;
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(188, 12);
            this.labelResult.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(0, 13);
            this.labelResult.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(143, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Wynik:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(143, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Droga:";
            // 
            // buttonBranchAndBound
            // 
            this.buttonBranchAndBound.Location = new System.Drawing.Point(12, 98);
            this.buttonBranchAndBound.Name = "buttonBranchAndBound";
            this.buttonBranchAndBound.Size = new System.Drawing.Size(100, 38);
            this.buttonBranchAndBound.TabIndex = 7;
            this.buttonBranchAndBound.Text = "Branch and Bound";
            this.buttonBranchAndBound.UseVisualStyleBackColor = true;
            this.buttonBranchAndBound.Click += new System.EventHandler(this.buttonBranchAndBound_Click);
            // 
            // buttonTabu
            // 
            this.buttonTabu.Location = new System.Drawing.Point(12, 142);
            this.buttonTabu.Name = "buttonTabu";
            this.buttonTabu.Size = new System.Drawing.Size(100, 38);
            this.buttonTabu.TabIndex = 8;
            this.buttonTabu.Text = "Tabu Search";
            this.buttonTabu.UseVisualStyleBackColor = true;
            this.buttonTabu.Click += new System.EventHandler(this.buttonTabu_Click);
            // 
            // textBoxTime
            // 
            this.textBoxTime.Location = new System.Drawing.Point(233, 154);
            this.textBoxTime.Name = "textBoxTime";
            this.textBoxTime.Size = new System.Drawing.Size(59, 20);
            this.textBoxTime.TabIndex = 9;
            this.textBoxTime.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxTime_KeyPress_1);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(118, 157);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Czas wykonania [ms]:";
            // 
            // buttonGenetic
            // 
            this.buttonGenetic.Location = new System.Drawing.Point(13, 186);
            this.buttonGenetic.Name = "buttonGenetic";
            this.buttonGenetic.Size = new System.Drawing.Size(99, 38);
            this.buttonGenetic.TabIndex = 11;
            this.buttonGenetic.Text = "Genetic";
            this.buttonGenetic.UseVisualStyleBackColor = true;
            this.buttonGenetic.Click += new System.EventHandler(this.buttonGenetic_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 231);
            this.Controls.Add(this.buttonGenetic);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxTime);
            this.Controls.Add(this.buttonTabu);
            this.Controls.Add(this.buttonBranchAndBound);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.labelRoad);
            this.Controls.Add(this.buttonBruteForce);
            this.Controls.Add(this.buttonSource);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Metody dokładne";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSource;
        private System.Windows.Forms.Button buttonBruteForce;
        private System.Windows.Forms.Label labelRoad;
        private System.Windows.Forms.Label labelResult;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonBranchAndBound;
        private System.Windows.Forms.Button buttonTabu;
        private System.Windows.Forms.TextBox textBoxTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonGenetic;
    }
}

