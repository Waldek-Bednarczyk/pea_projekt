﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PEA_projekt
{
    public partial class Form1 : Form
    {
        Matrix matrix;
        string path = "";

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSource_Click(object sender, EventArgs e)
        {
            //otwarcie okna wyboru pliku
            OpenFileDialog openFileDialog = new OpenFileDialog();
            //zastosowanie filtra wyszukiwan
            openFileDialog.Filter = "xml files (*.xml)|*.xml|txt files (*.txt)|*txt";

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    path = openFileDialog.FileName;
                    string tmp = path.Substring(path.Length - 3, 3);
                    if (tmp == "txt") matrix = new Matrix(path, false);
                    else matrix = new Matrix(path, true);
                }
                catch (Exception ee)
                {
                    MessageBox.Show("Nie udało się wczytać danych", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void buttonBruteForce_Click(object sender, EventArgs e)
        {
            if (path != "")
            {
                int[] vertexTab = new int[matrix.vertex];
                labelRoad.Text = "";
                for (int i = 0; i < matrix.vertex; i++)
                {
                    vertexTab[i] = i;
                }
                BruteForce bruteForce = new BruteForce(matrix.matrix);
                var tmp = bruteForce.CountBF(vertexTab);
                for (int i = 0; i < tmp.road.Length; i++)
                {
                    this.labelRoad.Text += tmp.road[i] + "-";
                }
                this.labelRoad.Text += tmp.road[0];
                this.labelResult.Text = tmp.result.ToString();
            }
            else MessageBox.Show("Brak danych \nWczytaj dane", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonBranchAndBound_Click(object sender, EventArgs e)
        {
            if (path != "")
            {
                labelRoad.Text = "";
                BranchAndBound branchAndBound = new BranchAndBound(matrix.matrix);
                var tmp = branchAndBound.CountBandB();
                for (int i = 0; i < tmp.road.Length; i++)
                {
                    this.labelRoad.Text += tmp.road[i] + "-";
                }
                this.labelRoad.Text += tmp.road[0];
                this.labelResult.Text = tmp.result.ToString();
            }
            else MessageBox.Show("Brak danych \nWczytaj dane", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void buttonTabu_Click(object sender, EventArgs e)
        {
            if ((textBoxTime.Text != "") && (Convert.ToInt32(textBoxTime.Text) < 180000))
            {
                if (path != "")
                {
                    int time = Convert.ToInt32(textBoxTime.Text);
                    labelRoad.Text = "";
                    TabuSearch tabu = new TabuSearch(matrix.matrix);
                    var tmp = tabu.CountTabuSearch(time);
                    for (int i = 0; i < tmp.road.Length; i++)
                    {
                        this.labelRoad.Text += tmp.road[i] + "-";
                    }
                    this.labelRoad.Text += tmp.road[0];
                    this.labelResult.Text = tmp.result.ToString();
                }
                else MessageBox.Show("Brak danych \nWczytaj dane", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else MessageBox.Show("Podaj odpowiednią wartość kryterium stopu", "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //pozwalaj na wprowadzenie jedynie cyfr
        private void textBoxTime_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void buttonGenetic_Click(object sender, EventArgs e)
        {

        }
    }
}