﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace PEA_projekt
{
    class Matrix
    {
        private string path;
        public int vertex { get; set; }
        public int[,] matrix;

        public Matrix(string pathway, bool version)
        {
            path = pathway;
            if (version)
            {
                this.Deserialize();
            }
            else this.Create();
        }

        private void Deserialize()
        {
            //stworzenie i zaladowanie do xml
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            //stworzenie listy krawedzi
            XmlNodeList elemList = doc.GetElementsByTagName("edge");
            //ilosc wierzcholkow
            vertex = doc.GetElementsByTagName("vertex").Count;
            //stworzenie macierzy sasiedztwa
            matrix = new int[vertex, vertex];
            //licznik rzedu
            int row=0;
            //petla odczytujaca wartosci drogi dla wierzcholkow
            for (int i = 0; i < elemList.Count; i++)
            {
                if(elemList.Count % (vertex-1) == 0) //asymetryczne
                {
                    if ((i % (vertex - 1) == 0) && (i != 0))
                        row++;
                }
                else //symetryczne
                {
                    if ((i % vertex == 0) && (i != 0)) row++;
                }

                //wartosc kolumny
                int tmp = Convert.ToInt32(elemList[i].InnerText);
                //obrobka danych pobranych z xml
                string attrVal = elemList[i].Attributes["cost"].Value;
                attrVal = attrVal.Replace('.', ',');
                //wpisanie w odpowiednia komorke tablicy
                matrix[row, tmp] = Convert.ToInt32(Double.Parse(attrVal, System.Globalization.NumberStyles.Float));
            }
        }

        private void Create()
        {
            using (StreamReader sr = new StreamReader(path))
            {
                vertex = Convert.ToInt16(sr.ReadLine());
                matrix = new int[vertex, vertex];
                for (int j=0; j<vertex; j++)
                {
                    string values = sr.ReadLine();
                    string[] numbersSplitted = values.Split(new char[] { }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < vertex; i++)
                    {
                        matrix[j,i] = Convert.ToInt32(numbersSplitted[i]);
                    }
                }
            }
        }
    }
}