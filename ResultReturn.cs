﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEA_projekt
{
    //klasa zwracana w kazdym z algorytmow
    class ResultReturn
    {
        public int result { get; set; }
        public int[] road { get; set; }
    }
}
