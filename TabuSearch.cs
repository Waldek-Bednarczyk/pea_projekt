﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PEA_projekt
{
    class TabuSearch
    {
        int[,] _matrix;
        int _vertices;
        int _value = 0;
        int[,] _swapList;
        List<int> bestFoundSolution = new List<int>();
        int[,] _tabuList;
        int _cadency;
        int iterations;

        //konstruktor klasy
        public TabuSearch(int[,] matrix)
        {
            _matrix = matrix;
            _vertices = _matrix.GetLength(0);
            _cadency = Convert.ToInt16(_vertices * 1.4); ;
            _tabuList = new int[_vertices, _vertices];
            _swapList = new int[_vertices, _vertices];
        }

        //glowna metoda wywolujaca algorytm Tabu
        public ResultReturn CountTabuSearch(int timing)
        {
            int newSolutionAfter = Convert.ToInt32(_vertices * 19 * 100);
            Tabu(newSolutionAfter, timing);
            ResultReturn resultReturn = new ResultReturn { result = _value, road = bestFoundSolution.ToArray() };
            return resultReturn;
        }

        //zamiana dwoch elementow list
        public static void Swap<T>(IList<T> list, int indexA, int indexB)
        {
            T tmp = list[indexA];
            list[indexA] = list[indexB];
            list[indexB] = tmp;
        }

        //obliczanie wartosci drogi
        private int CountRoad(List<int> list)
        {
            int road = 0;
            for (int i = 0; i < list.Count()-1; i++)
            {
                road += _matrix[list[i], list[i + 1]];
            }
            road += _matrix[list[list.Count() - 1], list[0]];
            return road;
        }

        //rozwiazanie poczatkowe generowane za pomoca metody zachlannej
        private void GenerateStartSet()
        {
            bestFoundSolution.Add(0);
            for(int i = 0; i < _vertices - 1; i++)
            {
                int nextNode = 0;
                int nextNodeValue = int.MaxValue;
                for (int j = 0; j < _vertices; j++)
                {
                    if ((_matrix[bestFoundSolution.Last(), j] < nextNodeValue) && (!bestFoundSolution.Contains(j)))
                    {
                        nextNode = j;
                        nextNodeValue = _matrix[bestFoundSolution.Last(), j];
                    }
                }
                bestFoundSolution.Add(nextNode);
                _value += nextNodeValue;
            }
            _value += _matrix[bestFoundSolution.Last(), 0]; //dodanie wartosci drogi od konca do wiercholka startowego
        }

        //kryterium aspiracji
        private bool Aspiration(int x, int y,List<int> list)
        {
            Swap(list, x, y);
            if (CountRoad(list) < _value)
            {
                Swap(list, x, y);
                return true;
            }
            Swap(list, x, y);
            return false;
        }

        //algorytm realizujacy nowe rozwiazanie pcozatkowe
        private List<int> Restart(List<int> list)
        {
            int swap = int.MaxValue;
            int first = 0;
            int second = 0;
            for (int j = 0; j < _vertices; j++)
            {
                for (int k = _vertices-1; k > j; k--)
                {
                    if ((_swapList[j, k] < swap) && (j != k))
                    {
                        swap = _swapList[j, k];
                        first = j;
                        second = k;
                    }
                }
            }
            _tabuList[first, second] += _cadency;
            _swapList[first, second]++;
            Swap(list, first, second);
            return list;
        }

        //algorytm tabu
        private void Tabu(int check,int time)
        {
            Random rnd = new Random();
            List<int> tmp = new List<int>();
            List<int> tmpBest = new List<int>();
            int tmpRoad = 0;
            //int tmpValue = 0;
            GenerateStartSet();
            int secondVertice = 0;
            int firstVertice = 0;
            tmp = bestFoundSolution;
            int countNoBetterSolution = 0;
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            for (; ; )
            {
                iterations++;
                int firstTmpRoad = int.MaxValue;
                for (int i = 0; i < _vertices; i++)
                {
                    for (int j = _vertices - 1; j > i; j--)
                    {
                        if ((_tabuList[i, j] == 0) || Aspiration(i, j, tmp))
                        {
                            Swap(tmp, i, j);
                            tmpRoad = CountRoad(tmp);
                            if (tmpRoad < firstTmpRoad)
                            {
                                firstTmpRoad = tmpRoad;
                                tmpBest = tmp.ToList();
                                firstVertice = i;
                                secondVertice = j;
                            }
                            Swap(tmp, i, j);
                        }
                    }
                }
                tmp = tmpBest.ToList();
                if (firstTmpRoad < _value)
                {
                    _value = firstTmpRoad;
                    bestFoundSolution = tmpBest.ToList();
                }
                else countNoBetterSolution++;//ilosc rozwiazan bez poprawy
                _swapList[firstVertice, secondVertice]++;
                _tabuList[firstVertice, secondVertice] += _cadency;//dodanie do listy Tabu
                //usuwanie z listy Tabu
                for (int j = 0; j< _vertices; j++)
                {
                    for(int k=_vertices-1; k>j; k--)
                    {
                        if (_tabuList[j, k] > 0) _tabuList[j, k]--;
                    }
                }

                if (countNoBetterSolution == check) //critical event
                {
                    countNoBetterSolution = 0;
                    tmp = Restart(tmp).ToList();
                }

                if (stopWatch.ElapsedMilliseconds > time)//sprawdzenie kryterium stopu
                {
                    stopWatch.Stop();
                    break;
                }
            }
        }
    }
}
